{{- define "base-RKE2ConfigTemplateSpec" }}
{{- $envAll := index . 0 -}}
{{- $machine_kubelet_extra_args := index . 1 -}}
{{- $machine_rke2_additionalUserData := (index . 2).additionalUserData -}}
{{- $machine_rke2_nodeLabels := (index . 2).nodeLabels -}}
{{- $machine_rke2_node_annotations := (index . 2).node_annotations -}}
{{- $machine_additional_files := index . 3 -}}

agentConfig:
{{- if $envAll.Values.cis_profile }}
  cisProfile: {{ $envAll.Values.cis_profile }}
{{- end }}
  additionalUserData:
    config: |{{ mergeOverwrite (dict) $envAll.Values.rke2.additionalUserData.config $machine_rke2_additionalUserData.config | toYaml | nindent 6 }}
    strict: {{ pluck "strict" $envAll.Values.rke2.additionalUserData $machine_rke2_additionalUserData | last | default false | toYaml }}
  nodeLabels:
    {{ range $node_label_key, $node_label_value := mergeOverwrite (deepCopy $envAll.Values.rke2.nodeLabels) ($machine_rke2_nodeLabels | default dict) }}
    - {{ printf "%s=%s" $node_label_key $node_label_value }}
    {{ end }}

    {{/* creating node labels with b64enc values, which would mutate to a node annotation through Kyverno ClusterPolicy */}}
    {{ range $node_label_key, $node_label_value := mergeOverwrite (deepCopy $envAll.Values.rke2.node_annotations) ($machine_rke2_node_annotations | default dict) }}
      {{- $node_label_value_b64e_syntax_compliant := $node_label_value | b64enc | replace "=" "-x" }}

      {{/*********** handle the "Valid label value must be 63 characters or less" K8s requirement
      (https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set) */}}
      {{- if gt (len $node_label_value_b64e_syntax_compliant) 63 }}

        {{/*********** Math behind it in python
        >>> b64enc="WyB7ICJwYXRoIjoiL3Zhci9saWIvbG9uZ2hvcm4iLCAiYWxsb3dTY2hlZHVsaW5nIjp0cnVlfSwgeyAibmFtZSI6ImZhc3Qtc3NkLWRpc2siLCAicGF0aCI6Ii9tbnQvZXh0cmEiLCAiYWxsb3dTY2hlZHVsaW5nIjpmYWxzZSwgInN0b3JhZ2VSZXNlcnZlZCI6MTA0ODU3NjAsICJ0YWdzIjpbICJzc2QiLCAiZmFzdCIgXSB9XQ=="
        >>> b64enc[0:63]
        'WyB7ICJwYXRoIjoiL3Zhci9saWIvbG9uZ2hvcm4iLCAiYWxsb3dTY2hlZHVsaW5'
        >>> b64enc[0:63][-63:]
        'WyB7ICJwYXRoIjoiL3Zhci9saWIvbG9uZ2hvcm4iLCAiYWxsb3dTY2hlZHVsaW5'
        >>>
        >>> b64enc[0:126]
        'WyB7ICJwYXRoIjoiL3Zhci9saWIvbG9uZ2hvcm4iLCAiYWxsb3dTY2hlZHVsaW5nIjp0cnVlfSwgeyAibmFtZSI6ImZhc3Qtc3NkLWRpc2siLCAicGF0aCI6Ii9tbn'
        >>> b64enc[0:126][-63:]
        'nIjp0cnVlfSwgeyAibmFtZSI6ImZhc3Qtc3NkLWRpc2siLCAicGF0aCI6Ii9tbn'
        >>>
        >>> b64enc[0:189]
        'WyB7ICJwYXRoIjoiL3Zhci9saWIvbG9uZ2hvcm4iLCAiYWxsb3dTY2hlZHVsaW5nIjp0cnVlfSwgeyAibmFtZSI6ImZhc3Qtc3NkLWRpc2siLCAicGF0aCI6Ii9tbnQvZXh0cmEiLCAiYWxsb3dTY2hlZHVsaW5nIjpmYWxzZSwgInN0b3JhZ2VSZXNlc'
        >>> b64enc[0:189][-63:]
        'QvZXh0cmEiLCAiYWxsb3dTY2hlZHVsaW5nIjpmYWxzZSwgInN0b3JhZ2VSZXNlc'
        >>>
        >>> b64enc[0:252]
        'WyB7ICJwYXRoIjoiL3Zhci9saWIvbG9uZ2hvcm4iLCAiYWxsb3dTY2hlZHVsaW5nIjp0cnVlfSwgeyAibmFtZSI6ImZhc3Qtc3NkLWRpc2siLCAicGF0aCI6Ii9tbnQvZXh0cmEiLCAiYWxsb3dTY2hlZHVsaW5nIjpmYWxzZSwgInN0b3JhZ2VSZXNlcnZlZCI6MTA0ODU3NjAsICJ0YWdzIjpbICJzc2QiLCAiZmFzdCIgXSB9XQ=='
        >>> len(b64enc) - 189
        59
        >>> b64enc[0:252][-59:]
        'nZlZCI6MTA0ODU3NjAsICJ0YWdzIjpbICJzc2QiLCAiZmFzdCIgXSB9XQ=='
        >>>          */}}

        {{- $node_label_value_b64e_parts := div (len $node_label_value_b64e_syntax_compliant) 63 | int }}
        {{- range $node_label_value_b64e_part_index, $e := until (add 1 $node_label_value_b64e_parts | int)  }}
          {{- $node_label_value_b64e_part_index := $node_label_value_b64e_part_index | int }}
          {{/* if we are at the last index, the number of chars we need to extract with the `trunc <negative nr>`
          from the total b64enc output can be lower than 63, so we calculate with: length of b64enc output - (last index) x 63 */}}
          {{- if eq $node_label_value_b64e_part_index $node_label_value_b64e_parts }}
            {{- $node_label_value_b64e_last_part_len := sub (len $node_label_value_b64e_syntax_compliant) (mul $node_label_value_b64e_part_index 63 | int) }}
    - {{ printf "%s=%s" (cat $node_label_key $node_label_value_b64e_part_index | nospace) ($node_label_value_b64e_syntax_compliant | trunc (mul (add 1 $node_label_value_b64e_part_index | int) 63 | int) | trunc (mul $node_label_value_b64e_last_part_len -1 | int)) }}
          {{- else }}
    - {{ printf "%s=%s" (cat $node_label_key $node_label_value_b64e_part_index | nospace) ($node_label_value_b64e_syntax_compliant | trunc (mul (add 1 $node_label_value_b64e_part_index | int) 63 | int) | trunc -63) }}
          {{- end }}
        {{- end }}
      {{- else }}
    - {{ printf "%s=%s" $node_label_key $node_label_value_b64e_syntax_compliant }}
      {{- end }}
    {{ end }}
  version: {{ $envAll.Values.k8s_version }}
  airGapped: {{ $envAll.Values.air_gapped }}
  kubelet:
    extraArgs:
      {{ range $kubelet_flag_key, $kubelet_flag_value := mergeOverwrite (deepCopy $envAll.Values.kubelet_extra_args) $machine_kubelet_extra_args }}
      - {{ printf "%s=%s" $kubelet_flag_key $kubelet_flag_value | quote }}
      {{ end }}
  {{- if $envAll.Values.ntp }}
  ntp:
{{ $envAll.Values.ntp | toYaml | indent 4 }} {{/* this line needs to have no leading spaces to ensure correct rendering */}}
  {{- end }}
preRKE2Commands:
  - echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.conf
  - echo "fs.inotify.max_user_instances = 512" >> /etc/sysctl.conf
  - sysctl --system
  {{- if and (not (eq $envAll.Values.capi_providers.infra_provider "capd")) $envAll.Values.dns_resolver }}
  - systemctl restart systemd-resolved
  {{- end }}
  {{- if $envAll.Values.proxies.http_proxy }}
  - export HTTP_PROXY={{ $envAll.Values.proxies.http_proxy }}
  - export HTTPS_PROXY={{ $envAll.Values.proxies.https_proxy }}
  - export NO_PROXY={{ $envAll.Values.proxies.no_proxy }}
  {{- end }}
files:
{{ $rke2ctfiles := list }}
{{ $rke2ctfiles = include "rke2_config_toml" $envAll | append $rke2ctfiles -}}
{{- if $envAll.Values.dns_resolver }}
    {{- $rke2ctfiles = include "resolv_conf" $envAll | append $rke2ctfiles -}}
{{- end }}
{{- if ($envAll.Values.registry_mirrors | dig "hosts_config" "") }}
    {{- $rke2ctfiles = include "registry_mirrors" $envAll | append $rke2ctfiles -}}
{{- end }}
{{- if $envAll.Values.proxies.http_proxy }}
    {{- $rke2ctfiles = include "rke2_agent_containerd_proxy" $envAll | append $rke2ctfiles -}}
{{- end }}
{{- $additional_files := mergeOverwrite (deepCopy $envAll.Values.additional_files) $machine_additional_files }}
{{- if $additional_files }}
    {{- $rke2ctfiles = tuple $envAll $additional_files | include "additional_files" | append $rke2ctfiles -}}
{{- end }}
{{- if $rke2ctfiles -}}
    {{- range $rke2ctfiles -}}
        {{ . | indent 2 }}
    {{- end }}
{{- else }}
        []
{{- end }}
{{- end }}
